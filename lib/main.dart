import 'package:flutter/material.dart';
import './question.dart';
import './answer.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final questions = const [
      {
        'questionText': "what\'s yout favorite color?",
        'answers': ['Black', 'Red', 'Green', 'White']
      },
      {
        'questionText': "what\'s yout favorite animal?",
        'answers': ['Rabbit', 'Snake', 'Elephan', 'Lion']
      },
      {
        'questionText': "what\'s yout favorite animal?",
        'answers': ['Max', 'Max', 'Max', 'Max']
      },
    ];

  var _questionIndex = 0;

  void _answerQuestion() {
    var aBool = true;
    aBool = false;
    if (_questionIndex < questions.length){
      print('We have more question!');
    } else {
      print('No more question!');
    }
    setState(() {
      _questionIndex = _questionIndex + 1;
    });
    print(_questionIndex);
  }

  @override
  Widget build(BuildContext context) {
    

    // var dummy = const ['Hello'];
    // dummy.add('Max');
    // print(dummy);
    // dummy = [];
    // questions = [];

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('My Aplication'),
        ),
        body: _questionIndex < questions.length ? Column(
          children: [
            Question(
              questions[_questionIndex]['questionText'],
            ),
            ...(questions[_questionIndex]['answers'] as List<String>)
                .map((answer) {
              return Answer(_answerQuestion, answer);
            }).toList()
          ],
        ): 
        Center(
          child: Text('Halo'),
        ),
      ),
    );
  }
}
